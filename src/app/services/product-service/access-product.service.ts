import { Injectable } from '@angular/core';
import { ProductService } from "./product.service";
import { Subject } from 'rxjs';
import { Product } from '../../model/product';

@Injectable({
  providedIn: 'root'
})
export class AccessProductService {

  constructor(private _productService:ProductService ) { }

  private data = new Subject();
  public data$ = this.data.asObservable();

  getProductId(){
      this._productService.getProduct().subscribe((res:any)=>{
        this.data.next(res);
      })    
  }

  setProduct(product:Product){
    this._productService.setProduct(product).subscribe((res:any)=>{
      this.data.next(res);
    });
  }


}
