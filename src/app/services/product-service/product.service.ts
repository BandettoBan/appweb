import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Conection } from '../../utility/conection';
import { Operation } from '../../utility/operation';
import { Observable } from 'rxjs';



@Injectable()
export class ProductService {

  constructor(private _http:HttpClient) { }

  
  getProduct():Observable<HttpResponse<Response>>{
    return this._http.get<Response>(`${Conection.URL}${Operation.getProductId}?id=1`,{observe:'response'});
  }

  setProduct(product:any):Observable<HttpResponse<Response>>{
    return this._http.post<Response>(`${Conection.URL}${Operation.setProduct}`,product,{observe:'response'});
  }



}
