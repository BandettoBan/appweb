import { Component, OnInit } from '@angular/core';
import { AccessProductService } from "../../../services/product-service/access-product.service";
import { Product } from '../../../model/product';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private _serviceProduct:AccessProductService) { }

  ngOnInit() {
    this.getProductId();
  }

  getProductId(){
    this._serviceProduct.getProductId();
    this._serviceProduct.data$.subscribe((res:any)=>{
    })
  }


  setProduct(){
    let product:Product = new Product(2,'Esperanza Gomez','Bogotá','Por el ortencio');
    this._serviceProduct.setProduct(product);
    this._serviceProduct.data$.subscribe((res:any)=>{

    })
  }




}
