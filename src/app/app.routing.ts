import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
const routes: Routes = [
    
    {
        path: 'product',
        loadChildren: './components/product/product.module#ProductModule',
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }
 
    ];

//export const routing = RouterModule.forRoot(routes,{useHash:true});

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }