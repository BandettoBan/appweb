export class Product{
    Id:number;
    Name:string;
    City:string;
    Description:string;

    constructor(id:number,name:string,city:string,description:string){
        this.Id = id,
        this.Name = name,
        this.City = city,
        this.Description = description
    }
}