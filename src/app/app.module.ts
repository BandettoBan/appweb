import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { AccessProductService } from "./services/product-service/access-product.service";
import { ProductService } from "./services/product-service/product.service";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AccessProductService,ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
